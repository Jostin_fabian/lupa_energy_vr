﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CameraPointer::Update()
extern void CameraPointer_Update_m03EFDAC6980037B33C39C2070279E57FDFA0E982 (void);
// 0x00000002 System.Void CameraPointer::.ctor()
extern void CameraPointer__ctor_mB1A747A1AEACE6E16A9D805F4F6EB409804E451E (void);
// 0x00000003 System.Void CardboardStartup::Start()
extern void CardboardStartup_Start_m3FCB9C6659C076D1D1142C9BDAE65996D09AB1B9 (void);
// 0x00000004 System.Void CardboardStartup::Update()
extern void CardboardStartup_Update_m9F3B419AD13A1BB0B8AAC2C46CC1415438B00186 (void);
// 0x00000005 System.Void CardboardStartup::.ctor()
extern void CardboardStartup__ctor_mF4E4BB92450A8F689ADF75C738D86EDD87F39202 (void);
// 0x00000006 System.Void ObjectController::Start()
extern void ObjectController_Start_mA8D89923C9057F3A7BC95D874FA47D34F2C4213B (void);
// 0x00000007 System.Void ObjectController::TeleportRandomly()
extern void ObjectController_TeleportRandomly_m0609E15B3460753A6538D1D7A0371DDA30E1B305 (void);
// 0x00000008 System.Void ObjectController::OnPointerEnter()
extern void ObjectController_OnPointerEnter_mFA06A1FFFA451B1321791A1AB6A52615AD531773 (void);
// 0x00000009 System.Void ObjectController::OnPointerExit()
extern void ObjectController_OnPointerExit_mAFAE51E71479EA14AE801E61082A3010BE90BC6C (void);
// 0x0000000A System.Void ObjectController::OnPointerClick()
extern void ObjectController_OnPointerClick_mFD2C131A2887535B624F8203A4F05B7DA6267369 (void);
// 0x0000000B System.Void ObjectController::SetMaterial(System.Boolean)
extern void ObjectController_SetMaterial_mF4DF2222B39EC502F91056938CFFAA13AD4DE4C1 (void);
// 0x0000000C System.Void ObjectController::.ctor()
extern void ObjectController__ctor_m7F22B01157517689B2328035A4B87C2DAD3478F8 (void);
// 0x0000000D System.Boolean VrModeController::get__isScreenTouched()
extern void VrModeController_get__isScreenTouched_m72936928FB8C5DF4268F76BF164F02D2B10F5646 (void);
// 0x0000000E System.Boolean VrModeController::get__isVrModeEnabled()
extern void VrModeController_get__isVrModeEnabled_mF66A05F1018FEDB94A7010E2E3F714B4C91F5F96 (void);
// 0x0000000F System.Void VrModeController::Start()
extern void VrModeController_Start_mE853D09E112F95B6725D81945FBF80BAC5E80F59 (void);
// 0x00000010 System.Void VrModeController::Update()
extern void VrModeController_Update_mEECA330B7CB845AFD609A6367AF8FB6A150FD210 (void);
// 0x00000011 System.Void VrModeController::EnterVR()
extern void VrModeController_EnterVR_m6F15FBEF8EC065C24764BA6CD70573822018013F (void);
// 0x00000012 System.Void VrModeController::ExitVR()
extern void VrModeController_ExitVR_m3559F6F2FBAE5D19998A9B51D0B22A765D451BFA (void);
// 0x00000013 System.Collections.IEnumerator VrModeController::StartXR()
extern void VrModeController_StartXR_m3E9FF326A5B4CB0E1EB19F1321EE5C4B3F90C9BC (void);
// 0x00000014 System.Void VrModeController::StopXR()
extern void VrModeController_StopXR_m9464911C38618B50214F493450600A39E7EFA11F (void);
// 0x00000015 System.Void VrModeController::.ctor()
extern void VrModeController__ctor_mA3E66288FFE87992A48CB7FA23AE0531311DFEA8 (void);
// 0x00000016 System.Void VrModeController/<StartXR>d__10::.ctor(System.Int32)
extern void U3CStartXRU3Ed__10__ctor_mF343AAB11ACC5EB4DC599EE2CB804CE31A56B9D9 (void);
// 0x00000017 System.Void VrModeController/<StartXR>d__10::System.IDisposable.Dispose()
extern void U3CStartXRU3Ed__10_System_IDisposable_Dispose_m3564EBC2B3A1FFF341D8064161C5E0609BD6A06F (void);
// 0x00000018 System.Boolean VrModeController/<StartXR>d__10::MoveNext()
extern void U3CStartXRU3Ed__10_MoveNext_m83FA5ABF40268E8BBFAC61A360F97F648EC841D0 (void);
// 0x00000019 System.Object VrModeController/<StartXR>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartXRU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m42D60279BF2FBE6A7B98DB1CAC2055C01BD24B58 (void);
// 0x0000001A System.Void VrModeController/<StartXR>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartXRU3Ed__10_System_Collections_IEnumerator_Reset_mB9FC26327447A2E4DA95BCADE6C7C48B25994317 (void);
// 0x0000001B System.Object VrModeController/<StartXR>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartXRU3Ed__10_System_Collections_IEnumerator_get_Current_mF8108A377FD1D678FB408319943C30A6AECBE607 (void);
// 0x0000001C System.Void CarController::FixedUpdate()
extern void CarController_FixedUpdate_m62A2DA259A4FA667F2A78CC2701E1A4601C4A4F7 (void);
// 0x0000001D System.Void CarController::GetInput()
extern void CarController_GetInput_m13F5FC2DF8E3708ED4E9C54D475DDF692376945F (void);
// 0x0000001E System.Void CarController::HandleMotor()
extern void CarController_HandleMotor_mE8973138355B368A9239C16DA68113B806C21DC1 (void);
// 0x0000001F System.Void CarController::ApplyBreaking()
extern void CarController_ApplyBreaking_m8F4CED40A069C557B2A31EBBD1C1F8507E79F8D6 (void);
// 0x00000020 System.Void CarController::HandleSteering()
extern void CarController_HandleSteering_mF516C45FE58FA3D4FB1A7C520B3C552DF40E9099 (void);
// 0x00000021 System.Void CarController::UpdateWheels()
extern void CarController_UpdateWheels_mD848267D86551E97979325D2D2BC0B1D42B9B433 (void);
// 0x00000022 System.Void CarController::UpdateSingleWheel(UnityEngine.WheelCollider,UnityEngine.Transform)
extern void CarController_UpdateSingleWheel_mEB40B788EFEC5FC24E4821DF96EA2071C3ACEECC (void);
// 0x00000023 System.Void CarController::.ctor()
extern void CarController__ctor_m180A0B729C25B52327B05362FB3A7AA7A53A9739 (void);
// 0x00000024 System.Void CarEngine::Start()
extern void CarEngine_Start_mD6440D491EA20A033C01CF8F93BCFB6158F21EC0 (void);
// 0x00000025 System.Void CarEngine::FixedUpdate()
extern void CarEngine_FixedUpdate_m9782936AD69DD23C7AAE920853169E2F5515F933 (void);
// 0x00000026 System.Void CarEngine::ApplySteer()
extern void CarEngine_ApplySteer_m99DA4020BD8431E5685AC96ADB1BBDD0DA7A3F1E (void);
// 0x00000027 System.Void CarEngine::Drive()
extern void CarEngine_Drive_m937E814A7914D8D3574645DF50FCB91063E506C2 (void);
// 0x00000028 System.Void CarEngine::CheckWaypointDistance()
extern void CarEngine_CheckWaypointDistance_m2D2BF144F8F50B3F493050B315BFECC3F2997B30 (void);
// 0x00000029 System.Void CarEngine::.ctor()
extern void CarEngine__ctor_m7D3FDF9EF3A89698D318FC0D33597BFE8FA0583B (void);
// 0x0000002A System.Void CameraCar::Start()
extern void CameraCar_Start_mDAB6F070B00C0AE3B89686B8E682455DD5F3E270 (void);
// 0x0000002B System.Void CameraCar::LateUpdate()
extern void CameraCar_LateUpdate_mB1938EEDA9E4882B032446FDC23BE074CD8BCD61 (void);
// 0x0000002C System.Void CameraCar::.ctor()
extern void CameraCar__ctor_m27DD37B8EAF60CD43E4A2ECBF6FDCFA083AE0352 (void);
// 0x0000002D System.Void MovementCar::Update()
extern void MovementCar_Update_m323DE59764356222976CC770FC95901FE14EE17A (void);
// 0x0000002E System.Void MovementCar::.ctor()
extern void MovementCar__ctor_m1C79CEA4F47DF6BC903FEEB16EE2293AE8D6F0AA (void);
// 0x0000002F System.Void HUDController::Start()
extern void HUDController_Start_mAF284C5BAF9E4F1E227F1D145DF32335D93018F3 (void);
// 0x00000030 System.Void HUDController::FixedUpdate()
extern void HUDController_FixedUpdate_m4444566C49D92AC75FCE3DA75E4A130F364E8E4C (void);
// 0x00000031 System.Void HUDController::VelocityControl()
extern void HUDController_VelocityControl_m85D0453D8F44A312E8E303A04CFF1C006DEB9981 (void);
// 0x00000032 System.Void HUDController::BateriaControl()
extern void HUDController_BateriaControl_mCD058B5341A29708746456243CF1E483978494A1 (void);
// 0x00000033 System.Void HUDController::GradosControl()
extern void HUDController_GradosControl_m65F721F4EDF48A5A519F4A0AB3EE22E18CF541EB (void);
// 0x00000034 System.Void HUDController::.ctor()
extern void HUDController__ctor_mB896F630035C0CAC01533CF6DDF50FF7EB580FBA (void);
// 0x00000035 System.Void HideCursor::Start()
extern void HideCursor_Start_m0079655BA85F277191C901A57BCABAEADBEDC9B7 (void);
// 0x00000036 System.Void HideCursor::Update()
extern void HideCursor_Update_m0110B4E396530603B044335484804CFD57768214 (void);
// 0x00000037 System.Void HideCursor::.ctor()
extern void HideCursor__ctor_mB76D6971FD822E9BBABBE81862E088E4BA16127E (void);
// 0x00000038 UnityEngine.Vector2 MouseMovement::GetInput()
extern void MouseMovement_GetInput_m324E26ED51A4F4AF1E568706F7159912F7126767 (void);
// 0x00000039 System.Single MouseMovement::ClampVerticalAngle(System.Single)
extern void MouseMovement_ClampVerticalAngle_mCB6C68486D1E841F18EEBE153529B7B1E4CF25D0 (void);
// 0x0000003A System.Void MouseMovement::Update()
extern void MouseMovement_Update_mACD41EE40411FF8E602CE2011E905F2DC4034C7B (void);
// 0x0000003B System.Void MouseMovement::.ctor()
extern void MouseMovement__ctor_mF41EEAECA0FF9F9491D7537725E11EFA626FF629 (void);
// 0x0000003C System.Void PathController::OnDrawGizmos()
extern void PathController_OnDrawGizmos_mDF9019572BBE578EA707072F8DF434A0ECFEE7C5 (void);
// 0x0000003D System.Void PathController::.ctor()
extern void PathController__ctor_m7C967AF6FDEDEBB4425C52DCC1B2652BDB1463E8 (void);
// 0x0000003E System.Void HeathenEngineering.UX.Samples.ToggleSetAnimatorBoolean::SetBoolean(System.Boolean)
extern void ToggleSetAnimatorBoolean_SetBoolean_m1F8CE490D155B5549C1B4E8D9E3858EF7478DB5C (void);
// 0x0000003F System.Void HeathenEngineering.UX.Samples.ToggleSetAnimatorBoolean::.ctor()
extern void ToggleSetAnimatorBoolean__ctor_m2B6D045212E686CFBD244E61F9132181BD2840B9 (void);
static Il2CppMethodPointer s_methodPointers[63] = 
{
	CameraPointer_Update_m03EFDAC6980037B33C39C2070279E57FDFA0E982,
	CameraPointer__ctor_mB1A747A1AEACE6E16A9D805F4F6EB409804E451E,
	CardboardStartup_Start_m3FCB9C6659C076D1D1142C9BDAE65996D09AB1B9,
	CardboardStartup_Update_m9F3B419AD13A1BB0B8AAC2C46CC1415438B00186,
	CardboardStartup__ctor_mF4E4BB92450A8F689ADF75C738D86EDD87F39202,
	ObjectController_Start_mA8D89923C9057F3A7BC95D874FA47D34F2C4213B,
	ObjectController_TeleportRandomly_m0609E15B3460753A6538D1D7A0371DDA30E1B305,
	ObjectController_OnPointerEnter_mFA06A1FFFA451B1321791A1AB6A52615AD531773,
	ObjectController_OnPointerExit_mAFAE51E71479EA14AE801E61082A3010BE90BC6C,
	ObjectController_OnPointerClick_mFD2C131A2887535B624F8203A4F05B7DA6267369,
	ObjectController_SetMaterial_mF4DF2222B39EC502F91056938CFFAA13AD4DE4C1,
	ObjectController__ctor_m7F22B01157517689B2328035A4B87C2DAD3478F8,
	VrModeController_get__isScreenTouched_m72936928FB8C5DF4268F76BF164F02D2B10F5646,
	VrModeController_get__isVrModeEnabled_mF66A05F1018FEDB94A7010E2E3F714B4C91F5F96,
	VrModeController_Start_mE853D09E112F95B6725D81945FBF80BAC5E80F59,
	VrModeController_Update_mEECA330B7CB845AFD609A6367AF8FB6A150FD210,
	VrModeController_EnterVR_m6F15FBEF8EC065C24764BA6CD70573822018013F,
	VrModeController_ExitVR_m3559F6F2FBAE5D19998A9B51D0B22A765D451BFA,
	VrModeController_StartXR_m3E9FF326A5B4CB0E1EB19F1321EE5C4B3F90C9BC,
	VrModeController_StopXR_m9464911C38618B50214F493450600A39E7EFA11F,
	VrModeController__ctor_mA3E66288FFE87992A48CB7FA23AE0531311DFEA8,
	U3CStartXRU3Ed__10__ctor_mF343AAB11ACC5EB4DC599EE2CB804CE31A56B9D9,
	U3CStartXRU3Ed__10_System_IDisposable_Dispose_m3564EBC2B3A1FFF341D8064161C5E0609BD6A06F,
	U3CStartXRU3Ed__10_MoveNext_m83FA5ABF40268E8BBFAC61A360F97F648EC841D0,
	U3CStartXRU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m42D60279BF2FBE6A7B98DB1CAC2055C01BD24B58,
	U3CStartXRU3Ed__10_System_Collections_IEnumerator_Reset_mB9FC26327447A2E4DA95BCADE6C7C48B25994317,
	U3CStartXRU3Ed__10_System_Collections_IEnumerator_get_Current_mF8108A377FD1D678FB408319943C30A6AECBE607,
	CarController_FixedUpdate_m62A2DA259A4FA667F2A78CC2701E1A4601C4A4F7,
	CarController_GetInput_m13F5FC2DF8E3708ED4E9C54D475DDF692376945F,
	CarController_HandleMotor_mE8973138355B368A9239C16DA68113B806C21DC1,
	CarController_ApplyBreaking_m8F4CED40A069C557B2A31EBBD1C1F8507E79F8D6,
	CarController_HandleSteering_mF516C45FE58FA3D4FB1A7C520B3C552DF40E9099,
	CarController_UpdateWheels_mD848267D86551E97979325D2D2BC0B1D42B9B433,
	CarController_UpdateSingleWheel_mEB40B788EFEC5FC24E4821DF96EA2071C3ACEECC,
	CarController__ctor_m180A0B729C25B52327B05362FB3A7AA7A53A9739,
	CarEngine_Start_mD6440D491EA20A033C01CF8F93BCFB6158F21EC0,
	CarEngine_FixedUpdate_m9782936AD69DD23C7AAE920853169E2F5515F933,
	CarEngine_ApplySteer_m99DA4020BD8431E5685AC96ADB1BBDD0DA7A3F1E,
	CarEngine_Drive_m937E814A7914D8D3574645DF50FCB91063E506C2,
	CarEngine_CheckWaypointDistance_m2D2BF144F8F50B3F493050B315BFECC3F2997B30,
	CarEngine__ctor_m7D3FDF9EF3A89698D318FC0D33597BFE8FA0583B,
	CameraCar_Start_mDAB6F070B00C0AE3B89686B8E682455DD5F3E270,
	CameraCar_LateUpdate_mB1938EEDA9E4882B032446FDC23BE074CD8BCD61,
	CameraCar__ctor_m27DD37B8EAF60CD43E4A2ECBF6FDCFA083AE0352,
	MovementCar_Update_m323DE59764356222976CC770FC95901FE14EE17A,
	MovementCar__ctor_m1C79CEA4F47DF6BC903FEEB16EE2293AE8D6F0AA,
	HUDController_Start_mAF284C5BAF9E4F1E227F1D145DF32335D93018F3,
	HUDController_FixedUpdate_m4444566C49D92AC75FCE3DA75E4A130F364E8E4C,
	HUDController_VelocityControl_m85D0453D8F44A312E8E303A04CFF1C006DEB9981,
	HUDController_BateriaControl_mCD058B5341A29708746456243CF1E483978494A1,
	HUDController_GradosControl_m65F721F4EDF48A5A519F4A0AB3EE22E18CF541EB,
	HUDController__ctor_mB896F630035C0CAC01533CF6DDF50FF7EB580FBA,
	HideCursor_Start_m0079655BA85F277191C901A57BCABAEADBEDC9B7,
	HideCursor_Update_m0110B4E396530603B044335484804CFD57768214,
	HideCursor__ctor_mB76D6971FD822E9BBABBE81862E088E4BA16127E,
	MouseMovement_GetInput_m324E26ED51A4F4AF1E568706F7159912F7126767,
	MouseMovement_ClampVerticalAngle_mCB6C68486D1E841F18EEBE153529B7B1E4CF25D0,
	MouseMovement_Update_mACD41EE40411FF8E602CE2011E905F2DC4034C7B,
	MouseMovement__ctor_mF41EEAECA0FF9F9491D7537725E11EFA626FF629,
	PathController_OnDrawGizmos_mDF9019572BBE578EA707072F8DF434A0ECFEE7C5,
	PathController__ctor_m7C967AF6FDEDEBB4425C52DCC1B2652BDB1463E8,
	ToggleSetAnimatorBoolean_SetBoolean_m1F8CE490D155B5549C1B4E8D9E3858EF7478DB5C,
	ToggleSetAnimatorBoolean__ctor_m2B6D045212E686CFBD244E61F9132181BD2840B9,
};
static const int32_t s_InvokerIndices[63] = 
{
	986,
	986,
	986,
	986,
	986,
	986,
	986,
	986,
	986,
	986,
	856,
	986,
	980,
	980,
	986,
	986,
	986,
	986,
	963,
	986,
	986,
	835,
	986,
	980,
	963,
	986,
	963,
	986,
	986,
	986,
	986,
	986,
	986,
	542,
	986,
	986,
	986,
	986,
	986,
	986,
	986,
	986,
	986,
	986,
	986,
	986,
	986,
	986,
	986,
	986,
	986,
	986,
	986,
	986,
	986,
	984,
	796,
	986,
	986,
	986,
	986,
	856,
	986,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	63,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
