using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDController : MonoBehaviour
{
    
    // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    
    // Controller - KM/H
    public WheelCollider wheelFL;
    public GameObject velocityHUD;

    private float currentSpeed;
    private float kmH;
    private int velocityTimePass = 1;
    private int velocityTimeRequired = 10;

    // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    
    // Controller - ENERGY
    public GameObject cocheBody;
    public GameObject bateriaIcon;
    public GameObject bateriaHUD;
    public Sprite bateria100;
    public Sprite bateria80;
    public Sprite bateria60;
    public Sprite bateria40;
    public Sprite bateria20;
    public Sprite bateriaEmpty;
    public float bateriaCantidad = 100f;

    public int bateriaTimePass = 1;
    private int bateriaTimeRequired = 1000;

    private bool isActive = true;
    private int isActiveTimePass = 1;
    private int isActiveTimeRequired = 50;

    // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    // Controller - Grados
    public GameObject gradosHUD;

    private int gradosTimePass = 1;
    private int gradosTimeRequired = 10;
    // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    private void Start() {
        bateriaHUD.GetComponent<TextMesh>().text = ((int)(bateriaCantidad)).ToString()+"%";
    }

    // Update is called once per frame
    private void FixedUpdate() {
        VelocityControl();
        BateriaControl();
        GradosControl();
    }

    private void VelocityControl(){
        currentSpeed = 2 * Mathf.PI * wheelFL.radius * wheelFL.rpm * 60 / 100;
        float rounded = UnityEngine.Mathf.Round(currentSpeed);
        kmH = rounded / 10;

        velocityTimePass++;
        if (velocityTimePass > velocityTimeRequired) {
            velocityHUD.GetComponent<TextMesh>().text = ((int)(kmH)).ToString()+" km/h";
            velocityTimePass = 0;
        }

    } 

    private void BateriaControl() {

        if (bateriaCantidad <= 100f && bateriaCantidad > 80f) {
            bateriaIcon.GetComponent<SpriteRenderer>().sprite = bateria100;
        }
        else if (bateriaCantidad <= 80f && bateriaCantidad > 60) {
            bateriaIcon.GetComponent<SpriteRenderer>().sprite = bateria80;
        }
        else if (bateriaCantidad <= 60f && bateriaCantidad > 40) {
            bateriaIcon.GetComponent<SpriteRenderer>().sprite = bateria60;
        }
        else if (bateriaCantidad <= 40f && bateriaCantidad > 20) {
            bateriaIcon.GetComponent<SpriteRenderer>().sprite = bateria40;
        }
        else if (bateriaCantidad <= 20f && bateriaCantidad > 0) {
            bateriaIcon.GetComponent<SpriteRenderer>().sprite = bateria20;
        }
        else {
            bateriaIcon.GetComponent<SpriteRenderer>().sprite = bateriaEmpty;
        }

        
        bateriaTimePass++;
        if (bateriaTimePass > bateriaTimeRequired) {
            bateriaHUD.GetComponent<TextMesh>().text = ((int)(bateriaCantidad)).ToString()+"%";
            bateriaTimePass = 0;
            if (bateriaCantidad > 0){
                bateriaCantidad--;
            }
        }

        if (bateriaCantidad == 0){
            cocheBody.GetComponent<Rigidbody>().Sleep();
            bateriaIcon.SetActive(false);
            velocityHUD.SetActive(false);
            bateriaHUD.SetActive(false);
            gradosHUD.SetActive(false);
        }
        else {
            isActiveTimePass++;
            if (bateriaCantidad < 20f && isActiveTimePass > isActiveTimeRequired){
                if (!isActive){
                    bateriaIcon.SetActive(true);
                    isActive = true;
                    isActiveTimePass = 0;
                }
                else if (isActive && isActiveTimePass > isActiveTimeRequired) {
                    bateriaIcon.SetActive(false);
                    isActive = false;
                    isActiveTimePass = 0;
                }
            }
        }
        
        
    }   

    private void GradosControl() {

        gradosTimePass++;
        if (gradosTimePass > gradosTimeRequired) {
            
            gradosTimePass = 0;
        }
    }
}
